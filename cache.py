from robot import Robot

class Cache:

    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        self.cache.setdefault(url, Robot(url))

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        print("Direcciones guardadas:")
        for url in self.cache:
            print(url)

    def content(self, direccion_url):
        self.retrieve(direccion_url)
        return self.cache[direccion_url].content()
