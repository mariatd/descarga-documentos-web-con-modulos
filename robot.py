import urllib.request

class Robot:

    def __init__(self, direccion_url):
        self.url = direccion_url
        self.descarga = False

    def retrieve(self):
        if not self.descarga:
            print ("Descargando url:", self.url)
            url_descargada = urllib.request.urlopen(self.url)
            self.content = url_descargada.read().decode('utf-8')
            self.descarga = True
    def show(self):
        print(self.content())

    def content(self):
        self.retrieve()
        return self.content

