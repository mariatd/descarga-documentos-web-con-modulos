from cache import Cache
from robot import Robot

print("Prueba clase robot")
robot = Robot('http://gsyc.urjc.es/')
print(robot.url)
robot.show()
robot.retrieve()
robot.retrieve()

print("Prueba clase cache")
cache = Cache()
cache.retrieve('http://gsyc.urjc.es/')
cache.show('https://labs.eif.urjc.es/')
cache.show_all()
